﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework;

namespace AppliedMathsAssessment
{
    class PopUp : Sprite
    {
        // ------------------
        // Data
        // ------------------
        Text popUpText;
        Button popUpButton;
        int buttonWidth;

        bool showing = false;
        float showTimePassed = 0;
        const float ANIMATION_TIME = 1f;
        Vector2 OffscreenPos;
        Vector2 OnscreenPos;

        // Interpolation data
        Vector2 begin;
        Vector2 change;
        Vector2 end;
        float duration;
        float time;

        // ------------------
        // Behaviour
        // ------------------
        public PopUp(
            Texture2D newTexture, 
            Texture2D newButtonTexture, 
            Texture2D newPressedTexture, 
            SpriteFont newButtonFont, 
            SoundEffect newClickSFX, 
            SpriteFont newPanelFont,
            string panelText,
            string buttonText,
            GraphicsDevice graphics)
            : base(newTexture)
        {
            popUpText = new Text(newPanelFont);
            popUpText.SetTextString(panelText);
            popUpText.SetAlignment(Text.TextAlignment.CENTRE);
            popUpText.SetColor(Color.Black);
            popUpButton = new Button(newButtonTexture, newPressedTexture, newButtonFont, newClickSFX);
            popUpButton.SetString(buttonText);
            buttonWidth = newButtonTexture.Width;

            OnscreenPos = new Vector2(
                graphics.Viewport.Bounds.Width / 2 - newTexture.Width / 2, 
                graphics.Viewport.Bounds.Height / 2 - newTexture.Height / 2);
            OffscreenPos = OnscreenPos;
            OffscreenPos.Y = 1000;

            SetPosition(OffscreenPos);
        }
        // ------------------
        public override void SetPosition(Vector2 newPosition)
        {
            position = newPosition;
            popUpText.SetPosition(position + new Vector2(texture.Width / 2, +50f));
            popUpButton.SetPosition(position + new Vector2(texture.Width / 2 - buttonWidth / 2, +100f));
        }
        // ------------------
        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);
            popUpText.Draw(spriteBatch);
            popUpButton.Draw(spriteBatch);
        }
        // ------------------
        public bool IsClicked()
        {
            return popUpButton.IsClicked();
        }
        // ------------------
        public void Show()
        {
            if (!showing)
            {
                showing = true;
                showTimePassed = 0;
            }
        }
        // ------------------
        public void Hide()
        {
            if (showing)
            {
                showing = false;
                SetPosition(OffscreenPos);
            }
        }
        // ------------------
        public void Update(GameTime gameTime)
        {
            float dt = (float)gameTime.ElapsedGameTime.TotalSeconds;

            if (showing)
            {
                // update timer
                showTimePassed += dt;

                ///////////////////////////////////////////////////////////////////

                // How long the transition will take in seconds
                duration = ANIMATION_TIME;

                // How long it has been since the start of the transition
                time = showTimePassed;

                time += (float)gameTime.ElapsedGameTime.TotalSeconds;

                // Check if our transition is over
                if (time <= duration)
                {
                    //Determine settings

                    // How long the transition will take in seconds
                    duration = ANIMATION_TIME;
                    
                    // Begin is the starting position for the transition
                    begin = OffscreenPos;

                    // End is our target position to end at (OnscreenPos)
                    end = OnscreenPos;

                    // THe direction and distance we will move by the end of the transition
                    change = end - begin;


                    // Cubic Easing Function
                    Vector2 k1 = -2 * change / duration * duration * duration;
                    Vector2 k2 = 3 * change / duration * duration;
                    Vector2 k3 = Vector2.Zero;
                    Vector2 k4 = begin;

                    //Plug in the equation
                    Vector2 newPos = k1 * time * time * time + k2 * time * time + k3 * time + k4;
                    SetPosition(newPos);
                }

                ///////////////////////////////////////////////////////////////////

                // OLD VERSION, DEPRECATED
                // Re-write this to move the pop-up using a cubic interpolation
                //SetPosition(OnscreenPos);

                ///////////////////////////////////////////////////////////////////  
                // END TASK 4 CODE
                ///////////////////////////////////////////////////////////////////  
            }

        }
        // ------------------
    }
}
